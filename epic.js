"strict"

!(function(){

  const EpicApplication= {
    imgList: [],
    imgTypes: ["aerosol","cloud","enhanced","natural"],
    dateCache: {},
    imgCache: {}

  }

  EpicApplication.imgTypes.forEach((typ)=> {
    EpicApplication.dateCache[typ]=null;
    EpicApplication.imgCache[typ]=new Map();
  })

  document.addEventListener("DOMContentLoaded", function (e)
  {

    //hi sir sorry for the poor variable choises.
    // i changed them multiple times beause the original variable names were even worst
    let infoList=[];
    let imageMenue=document.getElementById('image-menu');
    let template=document.getElementById('image-menu-item');
    let eid=document.getElementById('earth-image-date');
    let eit=document.getElementById('earth-image-title');
    let img=document.getElementById('earth-image');
    // let dateInput=document.getElementById("date");
    let imgTyp=document.getElementById("type");
    let date=document.getElementById("date");
    genOptionTyps(imgTyp,EpicApplication.imgTypes);

    imgTyp.addEventListener('change', function() {
          mostRecentDate(imgTyp,date,imageMenue);
        });
    
    

    const form=document.getElementById('request_form');
    form.addEventListener("submit",function (e){
      e.preventDefault();
      imageMenue=undefined;
      if(EpicApplication.imgCache[imgTyp.value].has(date.value))
      {
        displayImgCache(imgTyp.value,date.value,template,imageMenue);
      }
      else{
        fetch(`https://epic.gsfc.nasa.gov/api/${imgTyp.value}/date/${date.value}`)
        .then(response => { 
          if (!response.ok) {
            throw new Error("Not 2xx response",{cause: response});

          }
          return response.json(); 
        })

        .then(obj => {
          console.log("2) Deserialized response:", obj)
          // imageMenue.textContent = undefined;
          //i tried to display a message when there 
          if(obj.length===0)
          {
          //   imageMenue.textContent='';
          //   const parent=document.importNode(template.content,true);
          //   let child=parent.querySelector('span');
          //   child.parentNode.removeChild(child);

          //   parent.children[0].textContent="No Data Available";
          //   imageMenue.appendChild(parent);

          img.src="undefinded";
          eid.textContent=" ";
          eit.textContent=" ";

          }
          else{
            fetchToLi(imgTyp.value,date.value,imageMenue,template)
            imageMenue.addEventListener("click", function(e){
              e.preventDevault();

              if(e.tarfet.tagName=="span")
              {
                let arrDate=date.value.split("-");
                //.closest finds the closest "span" element to the parent
                let img1=e.target.closest("span").getAttribute("imageDateIndex");
                img.src=`https://epic.gsfc.nasa.gov/archive/${imgTyp.value}/${arrDate[0]}/${arrDate[1]}/${arrDate[2]}/jpg/${obj[img1].image}.jpg`;
                eit.textContent=obj[img1].caption;
                eid.textContent=obj[img1].date;
              }
            })
          }
          infoList=obj;
          displayDates(infoList,date.value);
          mostRecentDate(imgTyp.value);
        })
        .catch(error => { 
          console.log("3) Error:", error) 
        });
      }

    })

    function displayDates(infoList,originalDate)
    {
      imageMenue.textContent=undefined;
      infoList.forEach((oneDate, i)=>{
      const dateData=document.importNode(template.content,true);
      dateData.querySelector('li').setAttribute('data-image-list-index',i);
      dateData.querySelector('span').textContent=oneDate.date;
      dateData.querySelector('li').addEventListener('click', function(){
        displayImg(originalDate,oneDate);

        eid.textContent=oneDate.date;
        eit.textContent=oneDate.caption

      });

      imageMenue.appendChild(dateData);

      })
    }

    function displayImg(originalDate,oneDate)
    {
      let type=document.getElementById('type').value;
      let arr=originalDate.split('-');
      

      img.src=`https://epic.gsfc.nasa.gov/archive/${type}/${arr[0]}/${arr[1]}/${arr[2]}/jpg/${oneDate.image}.jpg`

    }

      function mostRecentDate(imgTyp,date,imageMenue)
      {
        if(EpicApplication.dateCache[imgTyp.value]!== null)
        {
          date.max=EpicApplication.dateCache[imgTyp];
        }
        else{
          fetch(`https://epic.gsfc.nasa.gov/api/${imgTyp.value}/all`)
          .then(response => { 
            
            if (!response.ok) {
              throw new Error("Not 2xx response",{cause: response});
    
            }
            return response.json(); 
          })
    
          .then(obj => { 
            console.log("2) Deserialized response:", obj);
            // imageMenue=undefined;
            let arrDates=obj.map((date) + new Date(date.date));
            arrDates.sort((a,b) => (b-a));
            let date1=arrDates[0];
            //toISOString() turns a date into a string format (YYYY-MM-DDTHH:mm:ss.sssZ) as explained in mdn web docs
            //i split("T") so i only get (YYYY-MM-DD) instead of (YYYY-MM-DDTHH:mm:ss.sssZ)
            let dateToString=date1.toISOString().split("T")[0];
            EpicApplication.dateCache[imgTyp.value]=dateToString;
          })
          .catch(error => { 
            console.log("3) Error:", error) 
          });
        }

      }

      function genOptionTyps(imgTyp,imgTypesArr)
      {
        imgTypesArr.forEach((typ) => {
          let option=document.createElement("option");
          option.setAttribute("value",typ);
          option.textContent=typ;
          imgTyp.appendChild(option);
        })
      }

      function displayImgCache(imgTypValue,dateValue,template,imageMenue)
      {
        const imgStored=EpicApplication.imgCache[imgTypValue].get(dateValue);
        if(imgStored){
          imageMenue.textContent=undefined;
          puttingLi(template,imageMenue,imgStored)
        }
      }
      //different from function displayDates
      function puttingLi(template,imageMenue,data)
      {
        EpicApplication.imgList=[];


        for (let i=0;i<data.length;++i)
        {
          EpicApplication.imgList.push(data[i].image);
          template=template.content;
          let content=template;
          let li=content.cloneNode(true).children[0];
          li.firstChild.textContent = data[i].date;
          li.firstChild.setAttribute("imageDateIndex", i);
          imageMenue.appendChild(li);
        }
      }

      function fetchToLi(imgTypValue,dateValue,imageMenue,template,obj)
      {
        EpicApplication.dateCache[imgTypValue]=dateValue;
        EpicApplication.imgCache[imgTypValue].set(dateValue,obj);
        // imageMenue.textContent=undefined;
        if (EpicApplication.imgCache[imgTypValue].has(dateValue)) {
          const img2 = EpicApplication.imgCache[imgTypValue].get(dateValue);
          EpicApplication.listOfImages = img2.map((img) => img.image);
          createLIS(img2, ulmenu, template);
        } else {
          createLIS(obj, imageMenue, template);
        }
      }
  })

}());